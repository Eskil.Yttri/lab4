package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain extends GameOfLife {
    /**
     * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
     * provided size
     *
     * @param height The height of the grid of cells
     * @param width  The width of the grid of cells
     */


    public BriansBrain(int rows, int columns) {
        super(rows, columns);
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }


    @Override
    public CellState getNextCell(int row, int col) {
        CellState state = getCellState(row, col);
        int livingNeighbors = countNeighbors(row, col, CellState.ALIVE);

        if (state.equals(CellState.ALIVE))
            state = CellState.DYING;
        else if (state.equals(CellState.DYING))
            state = CellState.DEAD;
        else if (state.equals(CellState.DEAD) && livingNeighbors == 2)
            state = CellState.ALIVE;

        return state;
    }
}
